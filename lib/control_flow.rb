# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char do |letter|
    if ("a".."z").include?(letter)
      str.delete!(letter)
    end
  end

  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.even?
    return str[(str.length / 2) - 1] + str[str.length / 2]
  else
    return str[str.length / 2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  number_of_vowels = 0
  VOWELS.each do |vowel|
    number_of_vowels += str.downcase.count(vowel)
  end

  number_of_vowels
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  string = ""
  arr.each do |el|
    if el == arr.last
      string << el
    else
      string << el + "#{separator}"
    end
  end

  string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.chars.each_index do |idx|
    if idx.even?
      str[idx] = str[idx].downcase
    else
      str[idx] = str[idx].upcase
    end
  end

  str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  new_string = []
  str.split.each do |word|
    if word.length >= 5
      new_string << word.reverse
    else
      new_string << word
    end
  end

  new_string.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  array = []
  (1..n).each do |number|
    if number % 3 == 0 && number % 5 == 0
      array << "fizzbuzz"
    elsif number % 3 == 0
      array << "fizz"
    elsif number % 5 == 0
      array << "buzz"
    else
      array << number
    end
  end

  array
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  array = arr.sort.reverse
  array
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  (2...num).each do |number|
    if num % number == 0
      return false
    end
  end

  return false if num == 1
  return true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  array = []
  (1..num).each do |number|
    array << number if num % number == 0
  end

  array.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  array = []
  (1..num).each do |number|
    array << number if num % number == 0 && prime?(number)
  end

  array.sort
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  array = []
  (1..num).each do |number|
    array << number if num % number == 0 && prime?(number)
  end

  array.length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odds = []
  evens = []
  arr.each do |el|
    evens << el if el.even?
    odds << el if el.odd?
  end

  return odds[0] if odds.length == 1
  return evens[0] if evens.length == 1
end
